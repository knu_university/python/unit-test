import  unittest

# 29 Write a function that finds elements in the sequence of integers that occur in it less often than
# others (but do occur).

def countIncome(arr):
    dictionary = dict()
    for i in arr:
        if dictionary.get(i) is None:
            dictionary[i] = 1
        else:
            dictionary[i] += 1
    return dictionary

def mostRare(arr):
    dictionary=countIncome(arr)
    minValue = min(dictionary.values())

    return [key for key,value in dictionary.items() if value==minValue]

# 42 The relation C between the sets A and B (A, B ⊂ Z) is given by a list of pairs. Implement functions thatcheck:
# a) whether the ratio C is functional; b) whether relation C is injective.

def isFunctional(arr:list):
    func=[el[0] for el in arr]
    return len(func)==len(set(func))

def isInjective(arr:list):
    func=[el[1] for el in arr]
    return len(func)==len(set(func))
    
#part 2
class TestFindRarest(unittest.TestCase):

    def test_parseToDictionary(self):
        self.array=(1,1,2,"a","a",1.0)
        expected={1:3,2:1,"a":2}
        self.assertEqual(countIncome(self.array),expected)

    def test_int(self):
        self.array=(1,1,11,1,4,4,5,5,5)
        self.assertEqual(mostRare(self.array),[11])

    def test_float(self):
        self.array=(1,1,11,1,5.1,4.0,1.0,4,4,4.3,4.3,5,5,5)
        self.assertEqual(mostRare(self.array),[11,5.1])

    def test_str(self):
        self.array=(1,1,11,1,5.1,4.0,1.0,"s","a","a",4,4,4.3,4.3,5,5,5)
        self.assertEqual(mostRare(self.array),[11,5.1,"s"])

class TestDependencies(unittest.TestCase):
    def setUp(self) -> None:
        self.testArray=[("a",3),("b",4),("c",1),("d",9),("e",6),("f",2),("g",5)]
        return super().setUp()

    def test_Functional_True(self):
        self.assertTrue(isFunctional(self.testArray))

    def test_Functional_False(self):

        self.testArray.append((("a",10)))
        self.assertFalse(isFunctional(self.testArray))

    def test_Injective_True(self):
        self.assertTrue(isInjective(self.testArray))

    def test_Injective_False(self):
        self.testArray.append((("a",3.0)))
        self.assertFalse(isInjective(self.testArray))


if __name__ == '__main__':
    unittest.main()